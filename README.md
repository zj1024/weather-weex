# Introduction

This project just use web component for now.

## Dependencies

> * Weex
> * Vue

####
How to use

1. clone
```
git clone https://gitee.com/zj1024/weather-weex.git
```

2. install packages
```
cnpm install
```

3. run on web
```
npm run serve
```

4. run on android
```
npm run android
```
