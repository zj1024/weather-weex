import Vue from 'vue'
/*global Vue*/
import Router from 'vue-router'
import Index from '@/components/Index/Index'

Vue.use(Router)

export const router = new Router({
  routes: [
    {
      path: '/',
      component: Index
    }
  ]
})
